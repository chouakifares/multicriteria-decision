# Multicriteria Decision 



## Project Description

This project aims to introduce you to the world of multicriteria decision making. The project will try to provide you the practical tools you can use to take  
- Decision in the presence of multiple criterias 
- Collective (Multiagent) Decision
- Decisions under uncertainty   


## Contributing
I am planning on working on this project on my own for the time being.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
I plan on making this an opensource project but I haven't licensed it yet. 

## Project status
In developement 
